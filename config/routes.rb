require 'api_constraints'

Rails.application.routes.draw do
  scope module: :api, defaults: { format: :json }, path: 'api' do
  	scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
	   devise_for :users,
		            path: '',
		            path_names: {
	              	sign_in: 'login',
	              	sign_out: 'logout',
	              	registration: 'signup'
	             	},
	             controllers: {
	               sessions: 'api/v1/users/sessions',
	               registrations: 'api/v1/users/registrations'
	             }
  	end
  end
end
