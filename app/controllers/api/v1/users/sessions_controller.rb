# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  before_action :authenticate!, :except => [:create, :destroy]

  respond_to :json

  # GET /resource/sign_in
  # def new
  #   super
  # end
  
  # POST /resource/sign_in
  def create 
    @user = User.where(email: params[:email]).first
    if @user&.valid_password?(params[:password])
      jwt = JWT.encode(
        {user_id: @user.id, exp: (Time.now + 2.weeks).to_i},
        Rails.application.secrets.secret_key_base,
        'HS256'
      )
      render :create, status: :created, locals: { token: jwt }
    else
      head(:unauthorized)
    end 
  end

  # DELETE /resource/sign_out
  def destroy
    current_user&.authentication_token = nil
    if current_user&.save
      head(:ok)
    else
      head(:unauthorized)
    end
  end
  
  private

  def respond_with(resource, _opts = {})
    render json: resource
  end

  def respond_to_on_destroy
    head :no_content
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
  # end
end
